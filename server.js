const express = require('express');
const uuid = require('uuid');
const os = require('os');

const port = process.env.PORT | 3000;
const hostname = os.hostname();
const app = express();

app.get('/', (req, res) => res.json({"uuid":uuid.v4(), "host":hostname}));

app.listen(port, () => console.log(`Listening on ${port}`));
